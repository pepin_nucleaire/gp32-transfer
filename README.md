<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!--
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">GP32 GPS Communication 3000</h3>
<!-- 
  <p align="center">
    Interface which can save and upload waypoint to Furuno GP32 using RS232
    <br />
    <a href="https://github.com/pepin_nucleaire/furuno-gpx-transfer"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/pepin_nucleaire/furuno-gpx-transfer">View Demo</a>
    ·
    <a href="https://github.com/pepin_nucleaire/furuno-gpx-transfer/issues">Report Bug</a>
    ·
    <a href="https://github.com/pepin_nucleaire/furuno-gpx-transfer/issues">Request Feature</a>
  </p>-->
</div>

<!-- TABLE OF CONTENTS -->
<!-- <details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details> -->

<!-- GETTING STARTED -->

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Installation

<!-- USAGE EXAMPLES -->

## Usage

<!-- ROADMAP -->

## Roadmap

- [x] ~~Save waypoints~~
  - [x] ~~Read from GP32~~
  - [x] ~~Save to GPX file compatible with OpenCPN~~
- [] Upload Waypoints
  - [x] ~~Convert GPX to NMEA~~
  - [x] ~~Upload NMEA to GP32~~
- [x] Make a nice app
  - [x] CLI
  - [] Web-based ?
  - [] Click-n-go ?

See the [open issues](https://github.com/pepin_nucleaire/furuno-gpx-transfer/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Your Name - [@juju_on_mini](https://twitter.com/juju_on_mini) - muller.julien.02@gmail.com.com

Project Link: [https://github.com/pepin_nucleaire/furuno-gpx-transfer](https://github.com/pepin_nucleaire/furuno-gpx-transfer)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

- [russkiy78 and it furunotogpx project](https://github.com/russkiy78/furunotogpx) that I used as a really big inspiration
- My dog

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
